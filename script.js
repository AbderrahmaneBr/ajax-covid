let httpReq = new XMLHttpRequest()
const table = document.querySelector('.table')
let countries = []

httpReq.open("GET", "https://api.covid19api.com/countries", true)

httpReq.onreadystatechange = ()=>{
    let state = httpReq.readyState
    let status = httpReq.status

    if(state==4 && status==200){
        let response = httpReq.response
        let JSONresponse = JSON.parse(response)
        JSONresponse.forEach(e => {
            countries.push([e.Slug, e.ISO2, e.Country])
        })
        countries.sort()
        countries.forEach(e => {
            table.innerHTML += '<tr><td onclick='+"'showInfo("+'"'+e[2]+'"'+")'"+'>'+`<div class='td_container'><img class='flags' src='https://computelab-staging.univ-reims.fr/static/pages/global/vendor/flag-icon-css/flags/1x1/${e[1].toLowerCase()}.svg' />`+e[2]+'</td></tr>'
        })

        //console.log(JSONresponse)
    }
}

httpReq.send()

function showInfo(Country){
    content.innerHTML = ""
    httpReq.open("GET", "https://api.covid19api.com/dayone/country/"+Country, true)
    httpReq.onreadystatechange = ()=>{
        let state = httpReq.readyState
        let status = httpReq.status

        if(state==4 && status==200){
            let response = httpReq.response
            let JSONresponse = JSON.parse(response)
            let recovered = [], active = [], deaths = []
            let i = 0
                JSONresponse.forEach(e => {
                        i += 1
                        content.innerHTML += '[' + e.Date + ']' + ' <span style="color:green;">Recovered: ' + e.Recovered + '</span>' + ' <span style="color:rgb(153, 187, 0);">Active: ' + e.Active + '</span>' + ' <span style="color:red;">Deaths: ' + e.Deaths + '</span>' + '<br>'
    
                        recovered.push({x: i, y: parseInt(e.Recovered, 10)})
                        active.push({x: i, y: parseInt(e.Active, 10)})
                        deaths.push({x: i, y: parseInt(e.Deaths, 10)})                    
                });
            let xlabels = countries.map((e, i)=> i)
            let myChart = new Chart("myChart",{
            type: "line",
            data: {
                labels: xlabels,
                datasets: [{
                    pointRadius: 3,
                    pointBackgroundColor: "green",
                    data: recovered
                },{
                    pointRadius: 3,
                    pointBackgroundColor: "blue",
                    data: active
                },{
                    pointRadius: 3,
                    pointBackgroundColor: "red",
                    data: deaths
                }]
            }
        } )
            
            //console.log(JSONresponse)
        }
    }

    httpReq.send()
}

